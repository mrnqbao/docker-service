#! /usr/bin/env /usr/local/bin/python2.7
#import scriptine
import os
import sys
import shutil
from shutil import make_archive
import tarfile
#import paramiko
import traceback
from SimpleXMLRPCServer import SimpleXMLRPCServer, SimpleXMLRPCRequestHandler
from SocketServer import ThreadingMixIn
import threading
import uuid
from subprocess import check_call
from subprocess import check_output


class DockerService:
    def div(self, x, y):
      return x // y

    def backup_aws():
      print 'Must be support Amazon Web Service'

    def backup(self, needBackupDir, archivename='test', uploaddir='/var/local/', backuphost='localhost', backupuser='root', backuppass='12345'):
      process_id = 'backup-%s' % uuid.uuid4()
      print 'START: %s' % process_id
      print '**********************************'
      print 'Backup Processing with information'
      print 'Backup Dir: %s' % needBackupDir
      print 'Upload Dir: %s' % uploaddir
      print 'Server Backup: %s' % backuphost
      print 'Server Backup User name: %s' % backupuser
      print 'Archive File: %s' % archivename
      print '**********************************'
      sftp = False
      ssh = False
      try:
        # create gtar in home directory tempolary
        archive_name = os.path.expanduser(os.path.join('~', archivename))
        archive_name = make_archive(archive_name, 'gztar', needBackupDir)

        # upload to server
        remote_file = os.path.join(uploaddir,os.path.basename(archive_name))
        ssh = paramiko.SSHClient()
        ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
        ssh.load_host_keys(os.path.expanduser(os.path.join("~", ".ssh", "known_hosts")))
        ssh.connect(backuphost, username=backupuser, password=backuppass)
        sftp = ssh.open_sftp()
        sftp.put(archive_name, remote_file)
        print 'Backup success'
        return {'return': True, 'data':remote_file,'message':'', 'process_id': process_id}
      except Exception as e:
        print 'Backup error: %s' % traceback.format_exc()
        return {'return': False, 'data':'','message': traceback.format_exc(), 'process_id': process_id}
      finally:
        if os.path.exists(archive_name):
          os.remove(archive_name)
        if sftp != False:
          sftp.close()
        if ssh != False:
          ssh.close()
        print 'END: %s' % process_id

    def restore(self, needRestoreDir, backupfile='/var/local/test.tar.gz', backuphost='localhost', backupuser='root', backuppass='12345'):
      process_id = 'restore-%s' % uuid.uuid4()
      print 'START: %s' % process_id
      print '**********************************'
      print 'Restore Processing with information'
      print 'Restore Dir: %s' % needRestoreDir
      print 'Backup Remote file: %s' % backupfile
      print 'Server Backup: %s' % backuphost
      print 'Server Backup User name: %s' % backupuser
      print '**********************************'
      sftp = False
      ssh = False
      tar = False
      try:
        archive_name = os.path.expanduser(os.path.join('~', os.path.basename(backupfile)))
        ssh = paramiko.SSHClient()
        ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
        ssh.load_host_keys(os.path.expanduser(os.path.join("~", ".ssh", "known_hosts")))
        ssh.connect(backuphost, username=backupuser, password=backuppass)
        sftp = ssh.open_sftp()
        sftp.get(backupfile, archive_name)

        tar = tarfile.open(archive_name)
        tar.extractall(needRestoreDir)
        print 'Restore success'
        return {'return': True, 'data':needRestoreDir, 'message':'', 'process_id': process_id}

      except Exception as e:
        print 'Restore error: %s' % traceback.format_exc()
        return {'return': False, 'data':'', 'message': traceback.format_exc(), 'process_id': process_id}
      finally:
        if os.path.exists(archive_name):
          os.remove(archive_name)
        if sftp != False:
          sftp.close()
        if ssh != False:
          ssh.close()
        if tar != False:
          tar.close()
        print 'END: %s' % process_id

    def docker_exec_cmd(self, containerId, cmd):
      process_id = 'docker-exec-cmd-%s' % uuid.uuid4()
      print 'START: %s' % process_id
      print '**********************************'
      print 'Docker Execute Command Processing with information'
      print 'ContainerId: %s' % containerId
      print 'Command: %s' % cmd
      print '**********************************'
      try:
        self.run('docker', 'exec', containerId, cmd)
        print 'Docker Execute Command success'
        return {'return': True, 'data':containerId, 'message':'', 'process_id': process_id}
      except Exception as e:
        print 'docker excecute command error: %s' % traceback.format_exc()
        return {'return': False, 'data':containerId, 'message': traceback.format_exc(), 'process_id': process_id}
      finally:
        print 'END: %s' % process_id

    def haproxy_config_loadbalacing(self, domain, nginx_ports):
      process_id = 'haproxy_config_loadbalacing-%s' % uuid.uuid4()
      ha_proxy_config_path = '/etc/haproxy/haproxy.cfg'
      print 'START: %s' % process_id
      print '**********************************'
      print 'HA Proxy Config Processing with information'
      print 'Domain: %s' % domain
      print 'nginx_ports: %s' % nginx_ports
      print 'HA Config file: %s' %  ha_proxy_config_path
      print '**********************************'
      try:
        # change . to _
        real_domain = domain
        domain = domain.replace('.', '_')

        # copy haproxy and edit on new file
        ha_proxy_config_path_bak = '%s.bak' % (ha_proxy_config_path)
        shutil.copy2(ha_proxy_config_path, ha_proxy_config_path_bak)

        define_hosts = '###define_hosts###'
        define_figure = '###firgure_out_which_one_to_use###'
        define_acl = "/%s/ a acl host_%s hdr(host) -i %s" % (define_hosts,domain,real_domain)
        backend_cluster = 'backend_%s_cluster' % (domain)
        define_cluster = "/%s/ a use_backend %s if host_%s" % (define_figure, backend_cluster, domain)

        nginx_ports = nginx_ports.split(',')
        user_backend = "$ a backend %s" % (backend_cluster)

        self.run('sed', '-i', define_acl, ha_proxy_config_path_bak)
        self.run('sed', '-i', define_cluster, ha_proxy_config_path_bak)

        self.run('sed', '-i', user_backend , ha_proxy_config_path_bak)
        self.run('sed', '-i', "$ a balance     roundrobin" , ha_proxy_config_path_bak)
        self.run('sed', '-i', "$ a cookie webpool prefix" , ha_proxy_config_path_bak)
        i = 1
        for nginx in nginx_ports:
          app = 'nginx%s' % i
          web_app = 'webpool-app%s' % i
          nginx_balance = "$ a server %s %s cookie %s check" % (app,nginx,web_app)
          self.run('sed', '-i', nginx_balance, ha_proxy_config_path_bak)
          i = i + 1

        #check config
        check_result = self.run_with_output('haproxy','-f',ha_proxy_config_path_bak,'-c')
        if  check_result == 'Configuration file is valid\n':
          # config success, update main file
          shutil.copy2(ha_proxy_config_path_bak, ha_proxy_config_path)
          self.run('service', 'haproxy', 'reload')
          print 'HA Proxy Config success'
          return {'return': True, 'data':domain, 'message':'', 'process_id': process_id}
        else:
          raise Exception('Config invalid')
      except Exception as e:
        print 'HA Proxy Config error: %s' % traceback.format_exc()
        return {'return': False, 'data':domain, 'message': traceback.format_exc(), 'process_id': process_id}
      finally:
        # remove new file
        if os.path.exists(ha_proxy_config_path_bak):
          os.remove(ha_proxy_config_path_bak)
        print 'END: %s' % process_id

    def run(self, app, *args):
      check_call([app] + list(args))

    def run_with_output(self, app, *args):
      return check_output([app] + list(args))


class DockerServiceHandler(SimpleXMLRPCRequestHandler):
  rpc_paths = ('/docker/v1',)

class ThreadedHTTPServer(ThreadingMixIn, SimpleXMLRPCServer):
    """Handle requests in a separate thread."""

if __name__ == '__main__':
    if sys.argv[1:]:
      port = int(sys.argv[1])
    else:
      port = 11000
    server = ThreadedHTTPServer(('', port), DockerServiceHandler)
    server.register_introspection_functions()
    server.register_instance(DockerService())
    print 'Starting server on port: %s, use <Ctrl-C> to stop' % (port)
    server.serve_forever()
    #scriptine.run()
